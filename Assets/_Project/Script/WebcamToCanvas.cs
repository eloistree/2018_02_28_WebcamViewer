﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamToCanvas : MonoBehaviour {
    public RawImage rawimage;
    public AspectRatioFitter ratioFitter;
    public int index=0;
   

    [Header("Debug")]
    public string webcamName;
    public Texture webcamTexture;

    private void Start()
    {
        DisplayWebcameName();
        SetWithWebcam(index);
    }


    private void Update()
    {
        if (webcamTexture == null) return;
        ratioFitter.aspectRatio = (float)webcamTexture.width / (float)webcamTexture.height;
        Debug.Log(string.Format("{0}x{1}", webcamTexture.width, webcamTexture.height));
    }

    public void NextWebcam() {
        index++;
        if (WebCamTexture.devices.Length <= index)
            index = 0;
        SetWithWebcam(index);

    }

    public void SetWithWebcam(int index)
    {
        SetWithWebcam( WebCamTexture.devices[index]);

    }

    public void SetWithWebcam(WebCamDevice device)
    {
        WebCamTexture webcamTexture = new WebCamTexture(device.name);
        webcamName = device.name;
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        this.webcamTexture = webcamTexture;
        Debug.Log(string.Format("{0}x{1}", webcamTexture.width, webcamTexture.height));
        
        webcamTexture.Play();

    } 
    public static void DisplayWebcameName()
    {
        WebCamDevice[] cam_devices = WebCamTexture.devices;

        for (int i = 0; i < cam_devices.Length; i++)
        {
            print("Webcam available: " + cam_devices[i].name);
        }

    }
}

